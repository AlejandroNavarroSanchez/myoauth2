package com.example.myoauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyOAuth2Application {

    public static void main(String[] args) {
        SpringApplication.run(MyOAuth2Application.class, args);
    }

}
